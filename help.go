//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// ----------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom
// the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// This file deals with built-in help and help topics. Since we don't want to
// have to deal with any console output package or spend time on formatting,
// all help content is preformatted for console width of 80 cells.
// To keep it easy, all this file is formatted for width of 80 cells.
// Please note that all other code in jstpp uses width 120 for code.
//

package main

import (
    "fmt"
    "os"
    "strings"
    "reflect"
    "sort"
)

// This is a long block of help topics.
const (

HELP_PREFIX = "\njstpp " + VERSION + " -- "

MAIN_HELP = `A simple indent-preserving template processor.

jstpp has good unicode support and is suitable for preprocessing YAML, JSight
and other formats with extensive use of indentation.

Usage:
 jstpp --help [topic]
 jstpp --version
 jstpp [options] <filename>

jstpp processes specified file line-by-line, substituting templates and
executing directives. Templates are by default enclosed in double curly braces
(but this can be overridden), directives are very similar to CPP directives
(#include, #define, #ifdef, etc.).

jstpp can also read from stdin: just pass a single dash as filename '-'.

jstpp works only with UTF-8 encoded documents. It emits whatever is considered
native encoding for your platform in Go build system. If you need to process
something other than UTF-8, use iconv to convert prior to processing.

Help topics are: directives errors examples builtins
                 define undef include if pragma warning error

Options follow CPP conventions: -fname=1 is same as -f name=1

Options:
 -Dname                  -- define a symbol with no value
 -Dname=value            -- define a symbol with value
 -Uname                  -- undefine a (predefined, built-in) symbol
 -fmax-include-depth=N   -- override max include depth (default is 200)
 -ftemplate-leadin=xx    -- set template start delimeter to specified character
                            sequence (default is '{{')
 -ftemplate-leadout=xx   -- set template end delimeter to specified character
                            sequence (default is '}}')
 -Hcondition=decision    -- set error handling options (see --help errors)
 --                      -- literal '--' stops option processing, the following
                            argument is NOT an option (can be used for
                            filenames starting with dash)

`
HELP_DIRECTIVES = `Directives

Each line that contains any amount of whitespace (including none), followed by
sharp (#) and then immediately by non-whitespace character, except '!' or '#',
is considered to contain a directive.

Lines with directives are consumed by template processor whether the directive
is valid or not. Valid directives are executed. Invalid directives produce
errors (this behaviour can be overridden).

Directives are similar to those of the C Preprocessor (but CPP has much more).

Available directives at a glance:

#define name        -- Defines a template with empty value
#define name value  -- Defines a template with specified value
#undef name         -- Undefine a previously defined template
#include filename   -- Include file at current indentation level
#ifdef name         -- Process the following lines only if template is defined
#ifndef name        -- Process the following lines if template is NOT defined
#else               -- Invert previously provided #ifxxx
#endif              -- Marks end of conditional (#ifxxx) block
#pragma once        -- This file should be included only once
#pragma tabsize=N   -- Define tab size for current file
#warning message    -- Explicitly create a warning with specified message
#error message      -- Explicitly create a warning with specified message

To get more detailed info on each directive, check out further help topics:
 define undef include if pragma warning error

`
HELP_ERRORS = `Errors

When processing, jstpp may encounter error conditions. They are handled 
according to the specified error handling rules. The rules may define error
conditions that are not fatal - that is, the error is reported to stderr,
but the processing continues, however the final exit code will be non-zero.

The exit codes are the following:
 0 -- successful completion of processing
 1 -- bad command line arguments
 2 -- syntax error (i.e unknown directives, unsupported pragmas etc.)
 3 -- execution error (i.e. file read failed)

Whenever multiple errors occur, the highest encountered error code is reported.

Error handling may be modified by -H option:
 jstpp -Hunsupported-pragma=fatal -H unknown-directive=fatal foo.txt

The syntax for -H option is:
 -Herror-condition=decision

Decisions are:
 ignore -- ignore specified condition and continue
 warn   -- warn about specified condition and continue
 error  -- produce error and continue (results in non-zero exit code)
 fatal  -- stop execution immediately

Error conditions:

`
HELP_BUILTINS = `Predefined templates and macros

Predefined macros in current version of jstpp:

`
// You may encounter malformed strings in the following fragment.
// They are actually NOT malformed. If they seem so, your viewer/IDE is failing
// to properly select or display monospace font for katakana, devanagari or
// whatever other Unicode we choose to use in examples.
HELP_EXAMPLES = `Examples

This section provides a number of examples of usage of jstpp. You can actually
run 'jstpp --help examples | jstpp -' to process this output with jstpp and 
look how these examples work.

!!! TODO: help section with examples not written yet

`
HELP_DEFINE = `#define and #undef directives

Syntax:
 #define name
 #define name value
 #define name \
   multiline value \
     with indentation
 #undef name

name can be any sequence of chars, but it can not contain template lead-out
sequence ('}}' by default). Unicode names are fully supported. If the name
should contain spaces, use double quotes.

If the value is not provided, empty string is used for the value.
If not quoted, value continues to the end of line excluding the trailing
whitespace.
Multi-line values are stored with indentation. First non-whitespace char 
defines the point of relative indentation within multiline value:

#define FOO \
  bar \
    baz \
      foobar
will store:
bar
  baz
    foobar

Redefinition of predefined names yields a warning by default, but is allowed.
Templates in value are resolved at the moment of definition.

`
HELP_INCLUDE = `#include directive

Syntax:
 #include path/to/file
 #include "path/to/file"

File content is included (processed) at the indentation level of the directive.

Example:
file1.txt contents:
---
This is file1
    This text is indented in file1 by 4 spaces
---
file0.txt contents:
---
The include below is indented by 4 spaces
    #include file1.txt
---
Processed text (jstpp file0.txt) looks like this:
---
The include below is indented by 4 spaces
    This is file1
        This text is indented in file1 by 4 spaces
---

When path is not quoted, it is considered to continue to the end of line
excluding the trailing whitespace.
<path/to/file> CPP-style includes are NOT supported.

`
HELP_IF = `#ifdef, #ifndef, #else, #endif

Syntax:
 #ifdef name
 #ifndef name
 #if macro
 #else
 #endif

#ifdef disables processing up to corresponding #else or #endif if the name is
NOT defined. #ifndef does the same if name IS defined.

#if resolves a macro (or a template) and checks whether it is truthy.
Argument to if should not have template lead-in and lead-out.

These directives can be stacked (pay attention to the order of #else's and 
#endif's).

name can be provided in double quotes if it has whitespace inside

`
HELP_PRAGMA = `#pragma directive

Since jstpp is to some extent compatible with CPP, we chose to use #pragma as
a way to provide special instructions to the template processor.

All #pragma commands are applied ONLY to the current file.

There is a small amount of subcommands for #pragma:


#pragma once

Indicates that the file should be processed only once, no matter how many times
it may get included. Subsequent includes just complete successfully as if the
file was empty.


#pragma tabsize=N

Provides a way to define tab size for the current file.

Tab size is used when the starting indentation of subsequent lines in multiline
define differs from indentation of first line.
In this case jstpp tries to calculate the correct monospace width of the indent
to have the correct inner indentation in the multiline template:
    ________________________________________________________
   |                                                        |
   | #define foo \                                          |
   |	This starting line is indented by a tab \           |
   |          But this second line is indented by 10 spaces |
   |________________________________________________________|

If tab size is 4, inner indent for the second line should be 6 cells (spaces).
However, if the tab size is 8, the inner indent should only be 2 cells.

`
HELP_ERROR_WARNING = `#error and #warning

Syntax:
 #error Message text up to the end of line
 #warning Message text up to the end of line

#error and #warning directives trigger the corresponding error and warning
conditions in the template processor.

The most common use for this is something like this:
#ifndef foo
#error Please define foo on the command line!
#endif

jstpp would then emit something like:
[error] file.txt:123: Please define foo on the command line!

`

)


func print_help(topic string) retcode {
    var page string
    rc := ESUCCESS

    if topic == "" {
        page = MAIN_HELP
    } else if strings.HasPrefix("directives", topic) {
        page = HELP_DIRECTIVES
    } else if strings.HasPrefix("errors", topic) {
        fmt.Fprintf(os.Stderr, "%s%s", HELP_PREFIX, HELP_ERRORS)
        _print_error_handling_table()
        return ESUCCESS
    } else if strings.HasPrefix("builtins", topic) {
        fmt.Fprintf(os.Stderr, "%s%s", HELP_PREFIX, HELP_BUILTINS)
        _print_builtins_info()
        return ESUCCESS
    } else if strings.HasPrefix("examples", topic) {
        page = HELP_EXAMPLES
    } else if strings.HasPrefix("define", topic) {
        page = HELP_DEFINE
    } else if strings.HasPrefix("undefine", topic) {
        page = HELP_DEFINE
    } else if strings.HasPrefix("include", topic) {
        page = HELP_INCLUDE
    } else if strings.HasPrefix(topic, "if") {
        page = HELP_IF
    } else if strings.HasPrefix("pragma", topic) {
        page = HELP_PRAGMA
    } else if strings.HasPrefix("error", topic) ||
              strings.HasPrefix("warning", topic) {
        page = HELP_ERROR_WARNING
    } else {
        page = MAIN_HELP
        rc = fatal_unknown_help_topic(topic)
    }
    fmt.Fprintf(os.Stderr, "%s%s", HELP_PREFIX, page)
    return rc
}


func _action_name(action action_t) string {
    return []string{"ignore","warn","error","fatal"}[int(action)]
}


func _print_error_handling_table() {
    max_name_len := len("Event")
    max_res_len := len("Default action")
    for name, _ := range events {
        if len(name) > max_name_len {
            max_name_len = len(name)
        }
    }
    max_name_len += 1
    max_res_len += 1
    fmtstr := fmt.Sprintf("%%-%ds  %%-%ds  %%s\n", max_name_len, max_res_len)
    fmt.Fprintf(os.Stderr, fmtstr, "Event", "Default action", "Description")
    fmt.Fprintf(os.Stderr, fmtstr, "-----", "--------------", "-----------")
    for id, handling := range events {
        fmt.Fprintf(os.Stderr, fmtstr, 
            id, _action_name(handling.default_action), handling.description)
    }
    fmt.Fprintf(os.Stderr, "\n")
}


func _print_builtins_info() {
    var names []string
    max_name_len := len("Builtin macro")
    for name, value := range template_namespace.builtins {
        desc := ""
        typename := reflect.TypeOf(value).String()
        if typename == "main.annotated_value" {
            desc = (value.(annotated_value)).description
            value = (value.(annotated_value)).value
            typename = reflect.TypeOf(value).String()
        }
        postfix := ""
        if typename == "main.macro" {
            postfix = "("
            for _, arg := range value.(macro).args {
                if postfix != "(" {
                    postfix += ","
                }
                postfix += arg
            }
            postfix += ")"
        } else if (typename == "main.builtin_macro") {
            postfix = "("
            for _, arg := range value.(builtin_macro).args {
                if postfix != "(" {
                    postfix += ","
                }
                postfix += arg
            }
            postfix += ")"
            desc = value.(builtin_macro).description
        }
        //prefix := ""
        //if !strings.HasPrefix(name, "__") {
        //    prefix = "  "
        //}
        name = name + postfix
        if len(name) > max_name_len {
            max_name_len = len(name)
        }
        names = append(names, name + "||" + desc)
    }
    max_name_len += 1
    fmtstr := fmt.Sprintf(" %%-%ds  %%s\n", max_name_len)
    fmt.Fprintf(os.Stderr, fmtstr, "Builtin macro", "Description")
    fmt.Fprintf(os.Stderr, fmtstr, "-------------", "-----------")
    sort.Strings(names)
    for _,name := range names {
        name, desc, _ := strings.Cut(name, "||")
        fmt.Fprintf(os.Stderr, fmtstr, name, desc)
    }
    fmt.Fprintf(os.Stderr,"\n")
}