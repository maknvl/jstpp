module gitlab.com/maknvl/jstpp

go 1.21.4

require (
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
)
