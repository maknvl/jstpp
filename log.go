//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// This file deals with logging in jstpp.
// We use a very simple logging / error reporting solution: everything goes into stderr in a simple predefined format.
//


package main


import (
    "fmt"
    "os"
)


// Error message format
//                ________________ left-aligned level tag
//               |   _____________ filename         
//               |  |   __________ line number
//               |  |  |   
const LOGFMT = "%s %s:%d: %s\n" // and message


// LogLevel denotes log level id for error logging
type loglevel int

const (
    DEBUG loglevel = iota
    WARNING
    ERROR
    FATAL
)


func leveltag(level loglevel) string {
    return [...]string{"[debug]","[warning]","[error]","[fatal]"}[int(level)]
}


func log_generic(level loglevel, filename string, line int, message string) {
    fmt.Fprintf(os.Stderr, LOGFMT, leveltag(level), filename, line, message)
}


func log_in_context(level loglevel, message string) {
    log_generic(level, path_make_relative(global.initial_cwd, context.path), context.linenum, message)
}


func log_error(message string) {
    log_in_context(ERROR, message)
}


func log_warning(message string) {
    log_in_context(ERROR, message)
}


func log_fatal(message string) {
    log_in_context(FATAL, message)
}


func log_debug(message string) {
    log_in_context(DEBUG, message)
}
