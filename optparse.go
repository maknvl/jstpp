//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//


package main


import (
    "os"
    "strings"
)


// Handlers for option parsing all have the same signature type
type _optparse_handler_t func(string, string) retcode


// Option parsing handlers map maps option prefix (-O) to corresponding
// handler function
var _optparse_handlers map[string]_optparse_handler_t


// Initialize mappings of option parsing handlers 
func optparse_init() {
    _optparse_handlers = make(map[string]_optparse_handler_t)
    _optparse_handlers["-f"] = _optparse_handle_param
    _optparse_handlers["-D"] = _optparse_handle_define
    _optparse_handlers["-U"] = _optparse_handle_undef
    _optparse_handlers["-H"] = _optparse_handle_events
}


func optparse() (string, retcode) {
    
    filename := ""
    context.linenum = 1
    rc := ESUCCESS

    for context.linenum < len(os.Args) {

        // Double dash means the end of options, therefore the next argument
        // must be the filename
        if os.Args[context.linenum] == "--" {
            context.linenum += 1
            if context.linenum < len(os.Args) {
                filename = os.Args[context.linenum]
            }
            break
        }

        // Single dash means that options ended and we are reading from stdin
        if os.Args[context.linenum] == "-" {
            filename = "-"
            break
        }

        // Otherwise everything starting with dash is an option, first one
        // without dash is a filename
        if strings.HasPrefix(os.Args[context.linenum],"-") {
            context.linenum, rc = _optparse_argument(context.linenum)
            if rc != ESUCCESS {
                return "", rc
            }
        } else {
            filename = os.Args[context.linenum]
            break
        }
    }

    return filename, rc
}


// _optparse_argument() takes current option position, parses it (including it's argument
// if required) and returns the index of the next argument or option and
// the return code, allowing to determine whether the option processing was
// successful
func _optparse_argument(pos int) (int, retcode) {
    // For each option we go through defined handlers to match
    for opt, handler := range _optparse_handlers {
        argstr, ok := strings.CutPrefix(os.Args[pos], opt)
        if ok {
            if os.Args[pos] == opt {
                // This is -O argument style option
                pos += 1
                if pos >= len(os.Args) {
                    // But there is no argument, that is an error
                    return pos, error_option_argument_missing()
                }
                argstr = os.Args[pos]
            }
            // Split the name=value string and run the handler
            name, value, _ := strings.Cut(argstr, "=")
            return pos+1, handler(name, value)
        }
    }
    // If no handlers were found, this is an unknown option here
    return pos+1, event_unknown_option()
}


// optparse_process_param sets the parameter value.
// When existing parameter value is integer, it expects the new value to be
// also an integer.
func _optparse_handle_param(name string, value string) retcode {
    // First off we check that parameter already exists
    switch parameters.set(name, value) {
    case PARAMETER_INVALID:
        return event_unknown_parameter(name)
    case PARAMETER_INVALID_VALUE:
        return event_invalid_parameter_value(name, value)
    case PARAMETER_READONLY:
        return event_readonly_parameter(name)
    }
    return ESUCCESS
}


// optparse_process_handling updates event handling rules
func _optparse_handle_events(event string, action string) retcode {
    act, ok := _optparse_action_from_string(action)
    if !ok {
        return event_invalid_action(event, action)
    }
    if !events_set_action(event, act) {
        return event_unknown_event(event)
    }
    return ESUCCESS
}


func _optparse_action_from_string(aname string) (action_t, bool) {
    switch aname {
    case "ignore":
        return ACTION_IGNORE, true
    case "warn":
        return ACTION_WARNING, true
    case "warning":
        return ACTION_WARNING, true
    case "error":
        return ACTION_ERROR, true
    case "fatal":
        return ACTION_FATAL, true
    }
    return ACTION_FATAL, false
}


func _optparse_handle_define(name string, value string) retcode {
    redef, success := templates_define(name, []string{value})
    if !success {
        return event_define_failed(name, value)
    }
       if redef {
        return event_builtin_redefined(name)
    }
    return ESUCCESS
}


func _optparse_handle_undef(name string, ignored_value string) retcode {
    if templates_undefine(name) {
        return event_builtin_undefined(name)
    }
    return ESUCCESS
}
