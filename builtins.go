//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file contains all built-in templates and macros
//a


package main


import (
    "time"
    "fmt"
    "os"
    "strconv"
)


func builtins_set_templates(builtins_map map[string]any) {
    
    builtins_map["__JSTPP__"] = annotated_value{ 
        value: VERSION,
        description: "jstpp version number" }

    builtins_map["__VERSION__"] = annotated_value{
        value: VERSION,
        description: "Alias for __JSTPP__"}
    
    builtins_map["__DATE__"] = annotated_value{
        value: builtin_DATE,
        description: "Current date in ISO 8601 format" }
    
    builtins_map["__TIME__"] = annotated_value{ 
        value: builtin_TIME,
        description: "Current localtime in HH:MM:SS format" }
    
    builtins_map["__FILE__"] = annotated_value{
        value: builtin_FILE,
        description: "Absolute path to current file without quotes"}
    
    builtins_map["__LINE__"] = annotated_value{
        value: builtin_LINE,
        description: "Current line number"}
    
    builtins_map["__COUNTER__"] = annotated_value{
        value: builtin_COUNTER,
        description: "Implements zero-based counter for each file"}
    
    builtins_map["__RESET_COUNTER__"] = annotated_value{
        value: builtin_RESET_COUNTER,
        description: "Resets the counter and returns 0"}
    
    builtins_map["__BASE_FILE__"] = annotated_value{
        value: builtin_BASE_FILE,
        description: "Name of the first file processed"}

    builtins_map["__TIMESTAMP__"] = annotated_value{
        value: builtin_TIMESTAMP,
        description: "Time of last modification of current file"}

    builtins_map["DEBUG"] = builtin_macro{ 
        args: []string{"..."},
        handler: builtin_DEBUG,
        description: "Print arguments one by one into debug log"}
    
    builtins_map["CAT"] = builtin_macro{
        args: []string{"..."},
        handler: builtin_CAT,
        description: "Concatenate arguments into a single string"}

    builtins_map["NIL"] = builtin_macro{
        args: []string{"..."},
        handler: builtin_NIL,
        description: "Disable output of arguments"}

    builtins_map["SEQ"] = builtin_macro{
        args: []string{"start", "end", "[step]", "[sep]"},
        handler: builtin_SEQ,
        description: "Produce sequence of numbers"}

    builtins_map["REPEAT"] = builtin_macro{
        args: []string{"times", "value"},
        handler: builtin_REPEAT,
        description: "Repeat value specified number of times"}

    builtins_map["EQ"] = builtin_macro{
        args: []string{"A","B"},
        handler: builtin_EQ,
        description: "Compares arguments"}

    builtins_map["IF"] = builtin_macro{
        args: []string{"cond","ifyes","[ifno]"},
        handler: builtin_IF,
        description: "Spreadsheet-like IF()"}

    builtins_map["IFNOT"] = builtin_macro{
        args: []string{"cond","ifno","[ifyes]"},
        handler: builtin_IFNOT,
        description: "Spreadsheet-like inversed IF()"}

    builtins_map["DEFINED"] = builtin_macro{
        args: []string{"name"},
        handler: builtin_DEFINED,
        description: "Check whether name is defined"}

    builtins_map["defined"] = builtin_macro{
        args: []string{"name"},
        handler: builtin_DEFINED,
        description: "Alias for DEFINED()"}

    builtins_map["AND"] = builtin_macro{
        args: []string{"..."},
        handler: builtin_AND,
        description: "Boolean AND across multiple args"}

    builtins_map["OR"] = builtin_macro{
        args: []string{"..."},
        handler: builtin_OR,
        description: "Boolean OR across multiple args"}

    builtins_map["XOR"] = builtin_macro{
        args: []string{"..."},
        handler: builtin_XOR,
        description: "Boolean XOR across multiple args"}

    builtins_map["NOT"] = builtin_macro{
        args: []string{"val"},
        handler: builtin_NOT,
        description: "Boolean negation"}

}


// CPP uses "Jan 22 2024" format for __DATE__. We use ISO 8601 after Go, but WITHOUT string quoting.
func builtin_DATE() string {
    return time.Now().Format(time.DateOnly)
}


// CPP uses "13:35:59" format for __TIME__. We use relaxed ISO 8601 after Go, but WITHOUT string quoting.
func builtin_TIME() string {
    return time.Now().Format(time.TimeOnly)
}

// CPP uses "relative/path/to/file" format for __FILE__. We use semi-absolute location, without quoting.
// stdin is denoted as '<stdin>', and during argument processing this value is '<args>'
func builtin_FILE() string {
    return context.path
}

// CPP uses modifiable line numbers that can be controlled by #line directive.
// During argument processing it contains the number of argument.
func builtin_LINE() string {
    return fmt.Sprintf("%d", context.linenum)
}

// GNU C Preprocessor (and those who follow - which is almost everybody) provides per-file non-resettable zero-based
// __COUNTER__. We do the same, but there is also __RESET_COUNTER__ that resets the count to 0.
func builtin_COUNTER() int {
    res := context.counter
    context.counter += 1
    return res
}


// Non-existent in C Preprocessors, but available in some template/macro processors
func builtin_RESET_COUNTER() int {
    context.counter = 1
    return 0
}


// GNU C Preprocessor provides quoted relative path in __BASE_FILE__. We provide unqouted semi-absolute.
func builtin_BASE_FILE() string {
    return global.base_file
}


// DEBUG functionality for printing arguments. Returns empty string as value.
func builtin_DEBUG(args [][]string) []string {
    for _,v := range args {
        log_debug(fmt.Sprintf("%q", v))
    }
    return []string{""}
}


// Concatenates input into a single string (useful for executable
// macro calls where templates are not available in string literals)
func builtin_CAT(args [][]string) []string {
    var out []string = args[0]
    i := 1
    for i < len(args) {
        out[len(out)-1] += args[i][0]
        j := 1
        for j < len(args[i]) {
            out = append(out, args[i][j])
            j += 1
        }
        i += 1
    }
    return out
}


// Same as GNU C Preprocessor __TIMESTAMP__ but unquoted
func builtin_TIMESTAMP() string {
    fi, err := os.Stat(context.path)
    if err != nil {
        return "!ERROR!"
    }
    return fi.ModTime().Format(time.ANSIC)
}


// Does nothing and returns empty string.
// Useful for preventing any output.
// NIL(__RESET_COUNTER__)
func builtin_NIL(ignored [][]string) []string {
    return []string{""}
}


func builtin_SEQ(args [][]string) []string {
    step := 1
    separator := ","
    start, err := strconv.Atoi(args[0][0])
    if err != nil {
        return []string{"!ERROR!"}
    }
    end, err := strconv.Atoi(args[1][0])
    if err != nil {
        return []string{"!ERROR!"}
    }
    if len(args) > 2 {
        step, err = strconv.Atoi(args[2][0])
    }
    if len(args) > 3 {
        separator = args[3][0]
    }
    i := start
    out := ""
    for i <= end {
        if out == "" {
            out += fmt.Sprintf("%d", i)
        } else {
            out += fmt.Sprintf("%s%d", separator, i)
        }
        i += step
    }
    return []string{out}
}


func builtin_REPEAT(args [][]string) []string {
    times, err := strconv.Atoi(args[0][0])
    if err != nil {
        return []string{"!ERROR!"}
    }
    var out []string = []string{""}
    for times > 0 {
        out[len(out)-1] += args[1][0]
        if len(args[1]) > 1 {
            out = append(out, args[1][1:]...)
        }
        times -= 1
    }
    return out
}


func _texts_are_same(a []string, b []string) bool {
    if len(a) != len(b) {
        return false
    }
    if a == nil {
        return true
    }
    for i,v := range a {
        if v != b[i] {
            return false
        }
    }
    return true
}

func builtin_EQ(args [][]string) []string {
    if _texts_are_same(args[0],args[1]) {
        return []string{"1"}
    }
    return []string{""}
}


func _bool(text []string) bool {
    // If text contains a single empty string or is empty, it is falsy.
    if len(text) == 0 {
        return false 
    }
    if len(text[0]) != 0 {
        return true
    }
    if len(text) == 1 {
        return false
    }
    return true
}


func builtin_IF(args [][]string) []string {
    if len(args) < 2 {
        return []string{""}
    }
    if _bool(args[0]) {
        return args[1]
    }
    if len(args) > 2 {
        return args[2]
    }
    return []string{""}
}


func builtin_IFNOT(args [][]string) []string {
    if len(args) < 2 {
        return []string{""}
    }
    if !_bool(args[0]) {
        return args[1]
    }
    if len(args) > 2 {
        return args[2]
    }
    return []string{""}
}


func builtin_DEFINED(args [][]string) []string {
    if templates_is_defined(args[0][0]) {
        return []string{"1"}
    }
    return []string{""}
}


func builtin_AND(args [][]string) []string {
    for _, arg := range args {
        if !_bool(arg) {
            return []string{""}
        }
    }
    return []string{"1"}
}

func builtin_OR(args [][]string) []string {
    ret := []string{""}
    for _, arg := range args {
        if _bool(arg) {
            ret[0] = "1"
            break
        }
    }
    return ret
}

func builtin_XOR(args [][]string) []string {
    ret := false
    for _, arg := range args {
        if _bool(arg) {
            if ret {
                ret = false
                break
            } else {
                ret = true
            }
        }
    }
    if ret {
        return []string{"1"}
    }
    return []string{""}
}

func builtin_NOT(args [][]string) []string {
    if _bool(args[0]) {
        return []string{""}
    }
    return []string{"1"}
}
