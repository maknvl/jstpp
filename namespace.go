//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file contains a quite generic variable namespace implementation that is used in jstpp templates
// 


package main


// A namespace contains builtin (lowest) level of variables and a stack of variable contexts
type namespace struct {
    builtins  map[string]any       // builtins are on the separate lowest level
    stack     []map[string]any     // stack contains a number of variable contexts
    stacksize int                  // stacksize is saved for easy access
    init_f    func(map[string]any) // initializer func is kept together to rebuild the namespace if needed
}


func (t *namespace) init(initializer func(map[string]any)) {
    t.builtins = make(map[string]any)
    t.init_f = initializer
    t.init_f(t.builtins)
    t.stack = append(t.stack,make(map[string]any))
    t.stacksize = 1
}


// Newly defined variables go to the current variable context. The method reports when they shadow the builtins.
func (t *namespace) define(name string, value any) bool {
    _, ret := t.builtins[name]
    t.stack[t.stacksize-1][name] = value
    return ret
}


// When undefined, variable is actually deleted from ALL stack and from builtins
func (t *namespace) undefine(name string) bool {
    _, ret := t.builtins[name]
    if ret {
        delete(t.builtins, name)
    }
    i := t.stacksize - 1
    for i >= 0 {
        _, ok := t.stack[i][name]
        if ok {
            delete(t.stack[i], name)
        }
        i -= 1
    }
    return ret
}


// When getting the variable, first the stack is searched from last to first and then the builtins.
func (t *namespace) get(name string) (any, bool) {
    i := t.stacksize - 1
    for i >= 0 {
        value, ok := t.stack[i][name]
        if ok {
            return value, true
        }
        i -= 1
    }
    value, ok := t.builtins[name]
    if ok {
        return value, true
    }
    return nil, false
}


// enter() creates a new variable context
func (t *namespace) enter() {
    t.stack = append(t.stack, make(map[string]any))
    t.stacksize += 1
}


// exit() closes a variable context
func (t *namespace) exit() {
    for k,_ := range t.stack[t.stacksize-1] {
        delete(t.stack[t.stacksize-1], k)
    }
    if t.stacksize > 1 {    
        t.stack[t.stacksize-1] = nil
        t.stack = t.stack[:t.stacksize-1]
        t.stacksize -= 1
    }
}


// clear_extra_layers() destroys all variable contexts, except for the first
func (t *namespace) clear_extra_layers() {
    for t.stacksize > 1 {
        t.exit()
    }
}


// reset() completely reinitializes the namespace
func (t *namespace) reset() {
    t.clear_extra_layers()
    t.exit()
    t.init_f(t.builtins)
}
