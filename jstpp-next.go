//
// jstpp is a simple indent-aware line-based unicode template processor.
// It is suitable for preprocessing YAML, JSight and other formats with extensive use of indentation.
//
// Usage:
//  jstpp --help [topic]
//  jstpp --version
//  jstpp [options] <filename>
//
// Topics: directives errors examples predefined define undef include if pragma warning error
//
// Options:
//  -Dname                  -- define symbol with no value
//  -Dname=value            -- define symbol with value
//  -Uname                  -- undefine symbol
//  -fmax-include-depth=N   -- override max include depth (default is 200)
//  -ftabsize=N             -- override default tab size (default is 4)
//  -ftemplate-leadin=xx    -- set template start delimeter (default is '{{')
//  -ftemplate-leadout=xx   -- set template end delimeter (default is '}}')
//  -Hcondition=decision    -- set error handling options (see --help errors)
//  --                      -- stop option processing
//
// Check the source code in help.go for more advanced usage information or run `jstpp --help`
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// To build this software, use Go build environment and run `go build`.
// 
// The source code has inline documentation. When developing, keep in mind that jstpp should handle unicode and
// indentation well, that's it's purpose.
// Otherwise have fun.
//
// Development tasks for current version:
//  - check and formalize grammar
//  - correctly handle bad macro defines
//  - add tests
//  - add examples and review user docs
//  - optimize processing speed
//  - add builtins: 
//      MAKESPACE(string), CELLWIDTH(string),
//      NEQ(A,B)
//      LT, LE, GT, GE, CONTAINS
//      ADD, SUB, DIV, MUL, MOD
//      DEFINE()???
//      EMBED
//  - ??? #ifdef __STDC__ recommendation
//  - ??? #pragma warning disable ?
//  - failed #ifxxx should still start if_stack (defaulting to condition failed)
//  - [1.0 blocker] remove redundant functionality
//  - [1.0 blocker] preserve custom endlines
//  - [1.0 blocker] streamlined single-pass parsing state-machine
//
package main


import (
	"os"
	"fmt"
	"slices"
	"bufio"
	"path"
	"strings"
	"strconv"
)


var parameters parameters_t 


var pragma_once_files []string


// initialize() performs required initialization of subcomponents
func initialize() {
	path_init()
	events_init()
	context_init()
	grammar_init()
	templates_init()
	parameters.init(func(para map[string]any) {
		para["max-include-depth"] = getset_wrap_int(&(global.max_include_depth))
		para["tabsize"]           = getset_wrap_int(&(global.tabsize))
		para["template-leadin"]   = getset_wrap_string(&templates_leadin)
		para["template-leadout"]  = getset_wrap_string(&templates_leadout)
	})
	optparse_init()
}


func print_version() retcode {
	fmt.Println("jstpp", VERSION)
	return ESUCCESS
}


// The main() routine is quite simple: parse cmdline options and 
// then pass control to either process_stdin() or process_file()
func main() {
	
	// If no command-line arguments are provided, we print the default help page and exit with error
	if len(os.Args) < 2 {
		print_help("")
		exit(EBADARGS)
	}

	// If --version command is supplied, we just print the version and exit
	if os.Args[1] == "--version" {
		exit(print_version())
	}

	// Otherwise we initialize everything and begin processing options
	initialize()
	context_set(global.initial_cwd, "<args>")

	// If --help command is supplied, we print help and exit
	if os.Args[1] == "--help" {
		topic := ""
		if len(os.Args) > 2 {
			topic = os.Args[2]
		}
		context.linenum = 2
		exit(print_help(topic))
	}

	filename, rc := optparse()

	switch filename {
	case "":
		// No filename provided is considered an error
        // (we don't read stdin by default)
        rc = fatal_filename_not_specified()
    case "-":
        // Single dash means we should read stdin
        context_pop()
        rc = process_stdin()
	default:
		// Otherwise we read from file
        context_pop()
        rc = process_file("", filename, global.initial_cwd, true)
	}

	exit(rc)
}


func process_stdin() retcode {
    // When processing stdin:
    // - our mock filename is "-"
    // - our working dir is derived not from the file path, but from initial_cwd
    // - our indent is none
    global.base_file = "-"
    context_set(global.initial_cwd, "-")
    defer context_pop()

    return process_from_filehandle(os.Stdin, "", "-", global.initial_cwd)
}


func process_file(indent string, filepath string, working_dir string, set_base_file bool) retcode {
    
    context_set(working_dir, filepath)
    defer context_pop()

    if set_base_file {
    	global.base_file = context.path
    }
    
    if slices.Contains(pragma_once_files, context.path) {
        return ESUCCESS
    }

    if context_get_include_depth() > global.max_include_depth {
        return fatal_include_depth_reached("-")
    }

    f, err := os.Open(context.path)
    if err != nil {
        // No matter what rc has returned, we can not continue with this file
        return event_file_open_failed()
    }
    defer f.Close()

    return process_from_filehandle(f, indent, context.path, context.workdir)
}


func process_from_filehandle(file *os.File, indent string, normpath string, wd string) retcode {
	
	var multiline_name string
	var multiline_indent_base string
	var multiline_value []string

	rc := ESUCCESS
	scanner := bufio.NewScanner(file)

	const (
		ST_REGULAR_LINE = iota
		ST_MULTILINE_DEFINE_FIRST_LINE
		ST_MULTILINE_DEFINE_CONTINUED
		ST_CONDITION_SKIP_TO_ELSE
		ST_CONDITION_SKIP_TO_ENDIF
	)
	state := ST_REGULAR_LINE
	context.linenum = 0
	multiline_starting_line := 0
	var if_stack []int
	if_counter := 0 
	for scanner.Scan() {

		line := scanner.Text()
		context.linenum += 1

		switch state {

		case ST_MULTILINE_DEFINE_FIRST_LINE:
			local_indent, value, to_be_continued := grammar_parse_multiline_define_line(line)
			multiline_value = nil
            multiline_value = append(multiline_value, value)
            if to_be_continued {
            	multiline_indent_base = local_indent
                state = ST_MULTILINE_DEFINE_CONTINUED
            } else {
                // this is a fake multi-line
                redef, _ := templates_define(multiline_name, multiline_value)
                if redef {
                	context_push()
                	context.linenum = multiline_starting_line
                    rc = event_builtin_redefined(multiline_name)
                    context_pop()
                    if rc != ESUCCESS {
                    	return rc
                    }
                }
                state = ST_REGULAR_LINE
            }

        case ST_MULTILINE_DEFINE_CONTINUED:
        	local_indent, value, to_be_continued := grammar_parse_multiline_define_line(line)
            diff, rc := get_indent_diff(local_indent, multiline_indent_base, multiline_name)
            if rc != ESUCCESS {
            	return rc
            }
            multiline_value = append(multiline_value, diff + value)
            if !to_be_continued {
                // this is a true multi-line
                redef, _ := templates_define(multiline_name, multiline_value)
                if redef {
                    context_push()
                	context.linenum = multiline_starting_line
                    rc = event_builtin_redefined(multiline_name)
                    context_pop()
                    if rc != ESUCCESS {
                    	return rc
                    }
                }
                state = ST_REGULAR_LINE
            }

        case ST_CONDITION_SKIP_TO_ELSE:
        	_, directive, _ := grammar_extract_directive(line)
            switch directive {
            case "ifdef":
                if_counter += 1
            case "ifndef":
                if_counter += 1
            case "else":
                if if_counter == 1 {
                    state = ST_REGULAR_LINE
                }
            case "endif":
                if_counter -= 1
                if if_counter == 0 {
                    state = if_stack[len(if_stack)-1]
                    if_stack = if_stack[:(len(if_stack)-1)]
                }
            }

        case ST_CONDITION_SKIP_TO_ENDIF:
        	_, directive, _ := grammar_extract_directive(line)
            switch directive {
            case "ifdef":
                if_counter += 1
            case "ifndef":
                if_counter += 1
            case "else":
                if if_counter == 0 {
                    rc = event_unexpected_else()
                    if rc != ESUCCESS {
                        return rc
                    }
                }
            case "endif":
                if_counter -= 1
                if if_counter == 0 {
                    state = if_stack[len(if_stack)-1]
                    if_stack = if_stack[:(len(if_stack)-1)]
                }
            }

		case ST_REGULAR_LINE:
			rc = ESUCCESS
			line_indent, directive, argstr := grammar_extract_directive(line)
			
			switch directive {

			// No directive in this line
			case "":
				for _, expanded_line := range templates_expand_line(line) {
					fmt.Println(indent + expanded_line)
				}

			// #error and #warning
			case "error":
				rc = handle_directive_error(argstr)
			case "warning":
				rc = handle_directive_warning(argstr)

			case "dump":
                log_debug(fmt.Sprintf("%q\n",template_namespace))

			// #include directive
            case "include":
            	rc = handle_directive_include(indent + line_indent, argstr)

            case "pragma":
            	rc = handle_directive_pragma(argstr)

            case "undef":
            	rc = handle_directive_undef(argstr)

            case "define":
            	multiline_name, rc = handle_directive_define(argstr)
            	if multiline_name != "" {
            		state = ST_MULTILINE_DEFINE_FIRST_LINE
            		multiline_starting_line = context.linenum
            	}

           	case "ifdef":
           		args := grammar_parse_directive_arguments(argstr, 1)
           		name := args[0]
                if name == "" {
                    rc = event_empty_if()
                } else {
                	ok := templates_is_defined(name)
                    if_stack = append(if_stack, state)
                    if !ok {
                        if_counter = 1
                        state = ST_CONDITION_SKIP_TO_ELSE
                    }
                }
            case "ifndef":
            	args := grammar_parse_directive_arguments(argstr, 1)
           		name := args[0]
                if name == "" {
                    rc = event_empty_if()
                } else {
                	ok := templates_is_defined(name)
                    if_stack = append(if_stack, state)
                    if ok {
                        if_counter = 1
                        state = ST_CONDITION_SKIP_TO_ELSE
                    }
                }
            case "if":
            	args := grammar_parse_directive_arguments(argstr, 0)
            	macro := args[0]
            	cond := false
            	if macro == "" {
            		rc = event_empty_if()
            	} else {
            		result, ok := _templates_expand_call(macro)
            		if !ok {
            			rc = event_if_macro_failed()
            		} else {
            			cond = _bool(result)
            		}
            	}
            	if_stack = append(if_stack, state)
            	if !cond {
            		if_counter = 1
            		state = ST_CONDITION_SKIP_TO_ELSE
            	}
            case "else":
            	if len(if_stack) == 0 {
                    rc = event_unexpected_else()
                } else {
                    if_counter = 1
                    state = ST_CONDITION_SKIP_TO_ENDIF
                }
            case "endif":
            	if_counter -= 1
                if len(if_stack) == 0 {
                    rc = event_unexpected_endif()
                } else {
                    state = if_stack[len(if_stack)-1]
                    if_stack = if_stack[:(len(if_stack)-1)]
                }

			// Unknown directive
			default:
				rc = event_unknown_directive(directive)
			} // switch directive

			if rc != ESUCCESS {
				return rc
			}

		} // switch state
	} // for
	
	if err := scanner.Err(); err != nil {
        return event_file_read_failed()
    }

    switch state {
    case ST_MULTILINE_DEFINE_FIRST_LINE:
    	rc = event_multiline_define_left_hanging(multiline_name)
    	if rc != ESUCCESS {
    		return rc
    	}
    	// We don't have the multiline body in this state
    	redef, _ := templates_define(multiline_name, []string{""})
    	if redef {
            context.linenum -= 1
            rc = event_builtin_redefined(multiline_name)
            context.linenum += 1
        }
    case ST_MULTILINE_DEFINE_CONTINUED:
    	rc = event_multiline_define_left_hanging(multiline_name)
    	if rc != ESUCCESS {
    		return rc
    	}
    	redef, _ := templates_define(multiline_name, multiline_value)
    	if redef {
    		context_push()
    		context.linenum = multiline_starting_line
    		rc = event_builtin_redefined(multiline_name)
    		context_pop()
    	}
    }

    if (len(if_stack) > 0) || (if_counter > 0) {
    	rc = event_if_left_hanging()
    }
    
    return rc
}


func handle_directive_error(argstr string) retcode {
	args := grammar_parse_directive_arguments(argstr, 0)
	return event_explicit_error(args[0])
}

func handle_directive_warning(argstr string) retcode {
	args := grammar_parse_directive_arguments(argstr, 0)
	return event_explicit_warning(args[0])
}

func handle_directive_pragma(argstr string) retcode {
	args := grammar_parse_directive_arguments(argstr, 1)
	arg0 := args[0]
	if arg0 == "once" {
		if context.path != "-" {
			pragma_once_files = append(pragma_once_files, context.path)
		}
	} else if strings.HasPrefix(arg0, "tabsize=") {
		s, _ := strings.CutPrefix(arg0, "tabsize=")
		iv, err := strconv.Atoi(s)
		if err != nil {
			return event_invalid_pragma_value(arg0)
		}
		if iv <= 1 {
			return event_invalid_pragma_value(arg0)
		}
		context.tabsize = iv
	}
	return ESUCCESS
}

func handle_directive_include(indent string, argstr string) retcode {
	args := grammar_parse_directive_arguments(argstr, 0)
	fn := args[0]
	if context.path == "-" {
	    return process_file(indent, fn, context.workdir, false)
	}
	return process_file(indent, fn, path.Dir(context.path), false)
}

func handle_directive_undef(argstr string) retcode {
	args := grammar_parse_directive_arguments(argstr, 1)
	name := args[0]
    if templates_undefine(name) {
        return event_builtin_undefined(name)
    }
    return ESUCCESS
}

func handle_directive_define(argstr string) (string, retcode) {
	args := grammar_parse_directive_arguments(argstr, 1)
	name := args[0]
	value := args[1]
	if name == "" {
		return "", event_empty_define()
	}
	if value == "\\" {
		return name, ESUCCESS
	}
	redef, _ := templates_define(name, []string{value})
	if redef {
		return "", event_builtin_redefined(name)
	}
	return "", ESUCCESS
}


func get_indent_diff(local_indent string, multiline_indent_base string, name string) (string, retcode) {
	rc := ESUCCESS
	diff := ""
    if !strings.HasPrefix(local_indent, multiline_indent_base) {
        rc = event_multiline_define_broken_indent(name)
        if rc == ESUCCESS {
            cwi := cellwidth(local_indent, context.tabsize)
            cwbase := cellwidth(multiline_indent_base, context.tabsize)
            if cwi < cwbase {
                diff = ""
                rc = event_multiline_define_shorter_indent(name)
                if rc != ESUCCESS {
                    return "", rc
                }
            } else if cwi == cwbase {
                diff = ""
            } else {
                diff = strings.Repeat(" ", cwi - cwbase)
            }
        }
    } else {
        diff, _ = strings.CutPrefix(local_indent, multiline_indent_base)
    }
    return diff, rc
}
