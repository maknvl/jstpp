//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// In this file we are dealing with global and per-file variable contexts
//


package main


// Global context holds app-global parameters
type global_context_t struct {
    tabsize           int    // tabsize is required in a rare case where jstpp needs to recreate some misused indent
    max_include_depth int    // max_include_depth defines the max level of file inclusion
    initial_cwd       string // path to initial working directory
    base_file         string // path to the first processed file (the one on the command-line)
}

var global global_context_t = global_context_t{
    tabsize:           4,    // Tabsize is preset to the most common editor size of 4
    max_include_depth: 200,  // Max include depth is set to 200 like in most C Preprocessors
    initial_cwd:       "",   // This gets filled when paths are initialized
    base_file:         "",   // This gets filled after command-line options are processed
}


// File context holds file-level parameters
type file_context_t struct {
    workdir string   // simulated working directory to process relative includes
    relpath string   // path to file as it was provided on the command line or in #include directive
    path    string   // normalized path (in most cases this will be absolute path to the file)
    linenum int      // current line number; while processing cmdline, this is set to the number of current argument
    tabsize int      // current effective tabsize, can be overridden by #pragma tabsize=
    counter int      // current counter value for __COUNTER__ builtin template
}

var context file_context_t
var context_stack []file_context_t


// Initial context is populated so that all functions using it would
// not choke up if called before actual initialization of file context
func context_init() {
    context.workdir = global.initial_cwd
    context.relpath = ""
    context.path = ""
    context.linenum = 0
    context.counter = 0
    context.tabsize = global.tabsize
}


// When a new context is set, previous context automatically gets pushed into context stack.
// This is used when including files.
func context_set(wd string, filepath string) {
    context_push()
    context.workdir = wd
    context.relpath = filepath
    context.path = path_normalize(filepath, wd)
    context.linenum = 0
    context.counter = 0
    context.tabsize = global.tabsize
}


// context_pop() is used to revert the context back after context set.
// The usual usage pattern would be:
// context_set(wd, filepath)
// defer context_pop()
func context_pop() {
    if len(context_stack) == 0 {
        log_generic(DEBUG, "", 0, "File context underflow!")
        exit(ESYSTEM)
    } else {
        context = context_stack[len(context_stack)-1]
        context_stack = context_stack[:len(context_stack)-1]
    }
}


// context_push() pushes current context into context stack without setting new context.
// This should normally not be used.
func context_push() {
    context_stack = append(context_stack, context)
}


// Since all includes use context_set(), the depth of includes is the same as the depth of context stack
func context_get_include_depth() int {
    return len(context_stack)
}
