//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file contains multiple parsing functions
//


package main


import (
	"unicode"
	"slices"
	"regexp"
	"strings"
	//"fmt"
)


const (
	// A line is considered to hold a directive in jstpp, when it has:
	//                  _____________________________ [1] Any amount of whitespace (including none)
    //                 |   __________________________ followed by sharp
    //                 |  |     _____________________ [2] followed by non-whitespace character, except exclamation mark
    //                 |  |    |                          or another sharp,
    //                 |  |    |       ______________ [2] optionally followed by any number of non-whitespace chars,
    //                 |  |    |      |     _________ [3] optionally
    //                 |  |    |      |    |    _____ [3] followed by whitespace and extra content
    //                 |  |    |      |    |   |    _ up to the end of the line.
    //                 |  |  __|__  __|__  | __|__ | 
    DIRECTIVE_RE = `^(\s*)#([^\s#!][^\s]*)($|\s+.*)$`
)

var directive_re *regexp.Regexp

func grammar_init() {
	directive_re = regexp.MustCompile(DIRECTIVE_RE)
}


func grammar_is_valid_identifier_first(r rune) bool {
	if unicode.IsDigit(r) {
		return false
	}
	return grammar_is_valid_identifier_next(r)
}

func grammar_is_valid_identifier_next(r rune) bool {
	if unicode.IsSpace(r) {
		return false
	}
	if slices.Contains([]rune{'#','"','(',')','\\',','},r) {
		return false
	}
	return true
}


func grammar_separate_indent(line string) (string, string) {
	runes := []rune(line)
	for i,r := range runes {
		if !unicode.IsSpace(r) {
			return string(runes[:i]), string(runes[i:])
		}
	}
	return line, ""
}


func grammar_split_macro_arguments(arglist string) ([]string, bool) {

	const (
		ST_SKIP_INITIAL_SPACE = iota
		ST_IN_IDENTIFIER
		ST_PAST_IDENTIFIER
		ST_IN_STRING_LITERAL
		ST_IN_NUMERIC_LITERAL
		ST_IN_ARGLIST
		ST_IN_ARGLIST_STRING
		ST_IN_ARGLIST_STRING_ESCAPE
		ST_PAST_ITEM
		ST_IN_STRING_LITERAL_ESCAPE
	)

	var out []string
	
	runes := []rune(arglist)
	start := 0
	end := 0
	counter := 0
	state := ST_SKIP_INITIAL_SPACE
	for i, r := range runes {

		switch state {
		
		case ST_SKIP_INITIAL_SPACE:
			if !unicode.IsSpace(r) {
				if grammar_is_valid_identifier_first(r) {
					start = i
					state = ST_IN_IDENTIFIER
				} else if r == rune('"') {
					start = i
					state = ST_IN_STRING_LITERAL
				} else if unicode.IsDigit(r) || (r == rune('-')) {
					start = i
					state = ST_IN_NUMERIC_LITERAL
				} else if r == rune(',') {
					out = append(out, "\"\"")
				} else {
					return nil, false
				}
			}

		case ST_IN_IDENTIFIER:
			if unicode.IsSpace(r) {
				end = i-1
				state = ST_PAST_IDENTIFIER
			} else if r == rune('(') {
				state = ST_IN_ARGLIST
				counter = 0
			} else if r == rune(',') {
				out = append(out, string(runes[start:i]))
				state = ST_SKIP_INITIAL_SPACE
			} else if !grammar_is_valid_identifier_next(r) {
				return nil, false
			}

		case ST_PAST_IDENTIFIER:
			if !unicode.IsSpace(r) {
				if r == rune('(') {
					state = ST_IN_ARGLIST
					counter = 0
				} else if r == rune(',') {
					out = append(out, string(runes[start:end+1]))
					state = ST_SKIP_INITIAL_SPACE
				} else {
					return nil, false
				}
			}

		case ST_IN_ARGLIST:
			if r == rune('(') {
				counter += 1
			} else if r == rune(')') {
				counter -= 1
				if counter < 0 {
					end = i
					state = ST_PAST_ITEM
				}
			} else if r == rune('"') {
				state = ST_IN_ARGLIST_STRING
			}

		case ST_IN_ARGLIST_STRING:
			if r == rune('\\') {
				state = ST_IN_ARGLIST_STRING_ESCAPE
			} else if r == rune('"') {
				state = ST_IN_ARGLIST
			}

		case ST_IN_ARGLIST_STRING_ESCAPE:
			state = ST_IN_ARGLIST_STRING

		case ST_PAST_ITEM:
			if !unicode.IsSpace(r) {
				if r == rune(',') {
					out = append(out, string(runes[start:end+1]))
					state = ST_SKIP_INITIAL_SPACE
				} else {
					return nil, false	
				}
			}

		case ST_IN_STRING_LITERAL:
			if r == rune('\\') {
				state = ST_IN_STRING_LITERAL_ESCAPE
			} else if r == rune('"') {
				end = i
				state = ST_PAST_ITEM
			}

		case ST_IN_STRING_LITERAL_ESCAPE:
			state = ST_IN_STRING_LITERAL

		case ST_IN_NUMERIC_LITERAL:
			if !unicode.IsDigit(r) {
				if unicode.IsSpace(r) {
					end = i-1
					state = ST_PAST_ITEM
				} else if r == rune(',') {
					res := string(runes[start:i])
					if res == "-" {
						return nil, false
					}
					out = append(out, res)
					state = ST_SKIP_INITIAL_SPACE
				} else {
					return nil, false
				}
			}
		} // switch
	} // for

	switch state {
	case ST_SKIP_INITIAL_SPACE:
		// this depends. If we already have something to output, we have the trailing comma.
		// That is fine.
		if len(out) > 0 {
			out = append(out, "\"\"")
			return out, true
		}
		return nil, false
	case ST_IN_IDENTIFIER:
		out = append(out, string(runes[start:]))
		return out, true
	case ST_PAST_IDENTIFIER:
		out = append(out, string(runes[start:end+1]))
		return out, true
	case ST_IN_STRING_LITERAL:
		// bad
	case ST_IN_STRING_LITERAL_ESCAPE:
		// bad
	case ST_IN_NUMERIC_LITERAL:
		res := string(runes[start:])
		if res != "-" {
			out = append(out, res)
			return out, true
		}
	case ST_IN_ARGLIST:
		// bad
	case ST_IN_ARGLIST_STRING:
		// bad
	case ST_IN_ARGLIST_STRING_ESCAPE:
		// bad
	case ST_PAST_ITEM:
		out = append(out, string(runes[start:end+1]))
		return out, true
	}
	return nil, false
}


func grammar_unqscape_string_literal(s string) (string, bool) {
	res, ok := grammar_unqscape_runes_literal([]rune(s))
	if !ok {
		return s, false
	}
	return string(res), true
}

func grammar_unqscape_runes_literal(runes []rune) ([]rune, bool) {
	
	const (
		ST_SEARCH_FOR_STARTING_DQUOTE = iota
		ST_IN_STRING
		ST_IN_STRING_ESCAPE
		ST_PAST_ENDING_DQUOTE
	)

	var out []rune

	state := ST_SEARCH_FOR_STARTING_DQUOTE

	for _, r := range runes {

		switch state {
		
		case ST_SEARCH_FOR_STARTING_DQUOTE:
			if !unicode.IsSpace(r) {
				if r != rune('"') {
					return runes, false
				}
				state = ST_IN_STRING
			}

		case ST_IN_STRING:
			if r == rune('\\') {
				state = ST_IN_STRING_ESCAPE
			} else if r == rune('"') {
				state = ST_PAST_ENDING_DQUOTE
			} else {
				out = append(out, r)
			}

		case ST_IN_STRING_ESCAPE:
			if r != rune('"') {
				out = append(out, rune('\\'))
			}
			out = append(out,r)
			state = ST_IN_STRING

		case ST_PAST_ENDING_DQUOTE:
			if !unicode.IsSpace(r) {
				return runes, false
			}
		} // switch
	} // for

	if state == ST_PAST_ENDING_DQUOTE {
		return out, true
	}
	return runes, false

}


func grammar_read_number(s string) (string, bool) {
	runes := []rune(s)

	const (
		ST_SKIP_INITIAL_SPACE = iota
		ST_DIGITS
		ST_PAST_DIGITS
	)

	start := 0
	end := 0
	state := ST_SKIP_INITIAL_SPACE
	for i, r := range runes {
		switch state {
		case ST_SKIP_INITIAL_SPACE:
			if !unicode.IsSpace(r) {
				if unicode.IsDigit(r) {
					start = i
					state = ST_DIGITS
				} else if r == rune('-') {
					start = i
					state = ST_DIGITS
				} else {
					return s, false
				}
			}
		case ST_DIGITS:
			if !unicode.IsDigit(r) {
				if unicode.IsSpace(r) {
					end = i-1
					state = ST_PAST_DIGITS
				} else {
					return s, false
				}
			}

		case ST_PAST_DIGITS:
			if !unicode.IsSpace(r) {
				return s, false
			}
		}
	}

	switch state {
	case ST_SKIP_INITIAL_SPACE:
		// bad
	case ST_DIGITS:
		res := string(runes[start:])
		if res != "-" {
			return res, true
		}
	case ST_PAST_DIGITS:
		res := string(runes[start:end+1])
		if res != "-" {
			return res, true
		}
	}
	return s, false
}


func grammar_extract_directive(line string) (string, string, string) {
	res := directive_re.FindStringSubmatch(line)
    if res == nil {
        return "","",""
    }
    return res[1],res[2],res[3]
}


func grammar_parse_directive_arguments(argstr string, amount int) []string {
	var out []string
	var s string

	const (
		SKIP_WHITESPACE = iota
		CHECK_FIRST_CHAR
		IN_QUOTED_STRING
		IN_QUOTED_STRING_ESCAPE
		IN_IDENTIFIER
		REASSESS
	)

	if amount > 0 {
		runes := []rune(argstr)
		state := SKIP_WHITESPACE
		have_escapes := false
		start := 0
		i := 0
		breakbreak := false
		for i < len(runes) {
			//fmt.Println("pda i:",i,"st:",state)
			switch state {
			case SKIP_WHITESPACE:
				if unicode.IsSpace(runes[i]) {
					i += 1
				} else {
					state = CHECK_FIRST_CHAR
				}
			case CHECK_FIRST_CHAR:
				if runes[i] == rune('"') {
					state = IN_QUOTED_STRING
					start = i+1
					have_escapes = false
				} else {
					state = IN_IDENTIFIER
					start = i
				}
				i += 1
			case IN_QUOTED_STRING:
				switch runes[i] {
				case rune('\\'):
					state = IN_QUOTED_STRING_ESCAPE
					have_escapes = true
				case rune('"'):
					// End of string
					if have_escapes {
						s = string(_unescape_rune_sequence(runes[start:i]))
					} else {
						s = string(runes[start:i])
					}
					out = append(out, s)
					state = REASSESS
				}
				i += 1
			case IN_QUOTED_STRING_ESCAPE:
				i += 1
				state = IN_QUOTED_STRING
			case IN_IDENTIFIER:
				if unicode.IsSpace(runes[i]) {
					// End of Identifier
					out = append(out, string(runes[start:i]))
					state = REASSESS
				}
				i += 1
			case REASSESS:
				amount -= 1
				if amount > 0 {
					state = SKIP_WHITESPACE
				} else {
					breakbreak = true
					break
				}
			}
			if breakbreak {
				break
			}
		}
		// Our cycle may have ended somewhere in the middle of non-final state
		switch state {
		case SKIP_WHITESPACE:     // Everything ended with whitespace, that's ok
		// case CHECK_FIRST_CHAR: // This can't actually happen
		case IN_QUOTED_STRING:
			if have_escapes {
				s = string(_unescape_rune_sequence(runes[start:]))
			} else {
				s = string(runes[start:])
			}
			out = append(out, s)
		case IN_QUOTED_STRING_ESCAPE:
			if have_escapes {
				s = string(_unescape_rune_sequence(runes[start:i-1]))
			} else {
				s = string(runes[start:i-1])
			}
			out = append(out, s)
		case IN_IDENTIFIER:
			out = append(out, string(runes[start:]))
		case REASSESS:
		}

		if i < len(runes) {
			argstr = string(_unquote_runes_if_quoted(runes[i:]))
		} else {
			argstr = ""
		}

		for amount > 0 {
			out = append(out, "")
			amount -= 1
		}
	} else {
		argstr = _unquote_if_quoted(argstr)
	}
	out = append(out, argstr)
	return out
}


func _unescape_rune_sequence(runes []rune) []rune {
	var out []rune
	escape := false
	for _,ch := range runes {
		if escape {
			if ch != rune('"') {
				out = append(out, rune('\\'))
			}
			out = append(out, ch)
			escape = false
		} else {
			out = append(out, ch)
		}
	}
	return out
}

func _unquote_runes_if_quoted(runes []rune) []rune {
	const (
		SKIP_LEADING_WHITESPACE = iota
		CHECK_FIRST_CHAR
		SEARCH_TRAILING_WHITESPACE
		IN_QUOTED_STRING
		IN_QUOTED_STRING_ESCAPE
		AFTER_QUOTED_STRING
	)
	var out []rune
	i := 0
	state := SKIP_LEADING_WHITESPACE
	start := 0
	end := -1
	have_escapes := false
	for i < len(runes) {
		switch state {
		case SKIP_LEADING_WHITESPACE:
			if unicode.IsSpace(runes[i]) {
				i += 1
			} else {
				state = CHECK_FIRST_CHAR
			}
		case CHECK_FIRST_CHAR:
			if runes[i] == rune('"') {
				state = IN_QUOTED_STRING
				start = i+1
			} else {
				state = SEARCH_TRAILING_WHITESPACE
				start = i
				end = i
			}
			i += 1
		case IN_QUOTED_STRING:
			switch runes[i] {
			case rune('\\'):
				state = IN_QUOTED_STRING_ESCAPE
				have_escapes = true
			case rune('"'):
				end = i-1
				state = AFTER_QUOTED_STRING
			}
			i += 1
		case IN_QUOTED_STRING_ESCAPE:
			i += 1
			state = IN_QUOTED_STRING
		case AFTER_QUOTED_STRING:
			if !unicode.IsSpace(runes[i]) {
				start -= 1
				state = SEARCH_TRAILING_WHITESPACE
				have_escapes = false
				end = i
			}
			i += 1
		case SEARCH_TRAILING_WHITESPACE:
			if !unicode.IsSpace(runes[i]) {
				end = i
			}
			i += 1
		}
	}
	switch state {
	case SKIP_LEADING_WHITESPACE:
	//case CHECK_FIRST_CHAR:
	case IN_QUOTED_STRING:
		start -= 1
		out = runes[start:]
	case IN_QUOTED_STRING_ESCAPE:
		start -= 1
		out = runes[start:]
	case AFTER_QUOTED_STRING:
		out = runes[start:end+1]
		if have_escapes {
			out = _unescape_rune_sequence(out)
		}
	case SEARCH_TRAILING_WHITESPACE:
		out = runes[start:end+1]
	}
	return out
}

func _unquote_if_quoted(s string) string {
	return string(_unquote_runes_if_quoted([]rune(s)))
}


func grammar_parse_multiline_define_line(line string) (string, string, bool) {
    ind, text := grammar_separate_indent(line)
    text = strings.TrimSpace(text)
    text, ok := strings.CutSuffix(text, "\\")
    if ok {
    	return ind, strings.TrimSpace(text), true
    }
    return ind, text, false
}
