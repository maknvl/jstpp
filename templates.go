//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file contains mechanisms used to define and resolve templates and macros
//


package main


import (
    "unicode"
    "strings"
    "reflect"
    "fmt"
)


// macro struct is used to hold user-defined macros
type macro struct {
    args []string
    code []string
}

// builtin_macro struct is used to hold guides for builtin macro functions.
// See builtins.go for the functions themselves
type builtin_macro struct {
    args        []string                  // Argument names are actually required only for generated user documentation
    handler     func([][]string) []string // The actual handler gets attached here
    description string                    // Description is also used only for generated docs
}


type annotated_value struct {
    value       any
    description string
}


var template_namespace namespace

var templates_leadin string = "{{"
var templates_leadout string = "}}"


// templares_define() creates a new template in the template namespace.
// Returns (builtin_macro_redefined, success) bool pair.
// See _templates_parse_callspec() for the possible failure causes.
func templates_define(callspec string, value []string) (bool, bool) {
    identifier, args, is_callable, ok := _templates_parse_callspec(callspec)
    if !ok {
        return false, false
    }
    if is_callable {
        // This is a callable macro. Expansion happens later.
        ok = template_namespace.define(identifier, macro{args:args,code:value})
    } else {
        // This is a regular "label". We must expand immediately.
        expanded := templates_expand_lines(value)
        ok = template_namespace.define(identifier, expanded)
    }
    return ok, true
}


// templates_undefine() removes a template from the template namespace.
func templates_undefine(name string) bool {
    return template_namespace.undefine(name)
}


func _templates_parse_callspec(callspec string) (string, []string, bool, bool) {
    
    const (
        SKIP_INITIAL_WHITESPACE = iota
        IN_IDENTIFIER
        ARGLIST_START
        IN_ARGLIST_IDENTIFIER
        ARGLIST_POST_COMMA
        ARGLIST_POST_IDENTIFIER
        POST_IDENTIFIER
    )

    var args []string
    runes := []rune(callspec)
    state := SKIP_INITIAL_WHITESPACE
    callable := false
    identifier_start := -1
    identifier_end := -1
    arg_start := -1

    for i,r := range runes {
        switch state {

        case SKIP_INITIAL_WHITESPACE:
            if !unicode.IsSpace(r) {
                if grammar_is_valid_identifier_first(r) {
                    state = IN_IDENTIFIER
                    identifier_start = i
                } else {
                    return callspec, nil, false, false
                }
            }

        case IN_IDENTIFIER:
            if !grammar_is_valid_identifier_next(r) {
                if unicode.IsSpace(r) {
                    identifier_end = i-1
                    state = POST_IDENTIFIER
                } else if r == rune('(') {
                    identifier_end = i-1
                    state = ARGLIST_START
                    callable = true
                } else {
                    return callspec, nil, false, false
                }
            }

        case ARGLIST_START:
            if !unicode.IsSpace(r) {
                if r == rune(')') { 
                    state = POST_IDENTIFIER
                } else if grammar_is_valid_identifier_first(r) {
                    state = IN_ARGLIST_IDENTIFIER
                    arg_start = i
                } else {
                    return callspec, nil, false, false
                }
            }

        case IN_ARGLIST_IDENTIFIER:
            if !grammar_is_valid_identifier_next(r) {
                if unicode.IsSpace(r) {
                    args = append(args, string(runes[arg_start:i]))
                    state = ARGLIST_POST_IDENTIFIER
                } else if r == rune(',') {
                    args = append(args, string(runes[arg_start:i]))
                    state = ARGLIST_POST_COMMA
                } else if r == rune(')') {
                    args = append(args, string(runes[arg_start:i]))
                    state = POST_IDENTIFIER
                } else {
                    return callspec, nil, false, false
                }
            }

        case ARGLIST_POST_IDENTIFIER:
            if !unicode.IsSpace(r) {
                if r == rune(',') {
                    state = ARGLIST_POST_COMMA
                } else if r == rune(')') {
                    state = POST_IDENTIFIER
                } else {
                    return callspec, nil, false, false
                }
            }

        case ARGLIST_POST_COMMA:
            if !unicode.IsSpace(r) {
                if grammar_is_valid_identifier_first(r) {
                    arg_start = i
                    state = IN_ARGLIST_IDENTIFIER
                } else {
                    return callspec, nil, false, false
                }
            }

        case POST_IDENTIFIER:
            if !unicode.IsSpace(r) {
                return callspec, nil, false, false
            }
        }
    }

    switch state {
    case IN_IDENTIFIER:
        identifier := string(runes[identifier_start:])
        return identifier, nil, false, true
    case POST_IDENTIFIER:
        identifier := string(runes[identifier_start:identifier_end+1])
        return identifier, args, callable, true
    }

    return callspec, nil, false, false
}


func templates_expand_lines(lines []string) []string {
    var out []string
    for _,line := range lines {
        out = append(out, templates_expand_line(line)...)
    }
    return out
}

func templates_expand_line(line string) []string {
    var out []string

    // We are using Cut to slice around 

    const (
        STATE_LOOK_FOR_LEADIN = iota
        STATE_IN_TEMPLATE
    )

    state := STATE_LOOK_FOR_LEADIN
    out = append(out,"")
    for {
        switch state {
        
        case STATE_LOOK_FOR_LEADIN:
            // Looking for lead-in
            before, after, found := strings.Cut(line, templates_leadin)
            if !found {
                out[len(out)-1] += line
                return out
            }
            out[len(out)-1] += before
            line = after
            state = STATE_IN_TEMPLATE
        
        case STATE_IN_TEMPLATE:
            before, after, found := strings.Cut(line, templates_leadout)
            if !found {
                out[len(out)-1] += templates_leadin + line
                return out
            }
            line = after
            result, found := templates_expand_template(before)
            if !found {
                out[len(out)-1] += templates_leadin + before + templates_leadout
            } else {
                if len(result) == 1 {
                    out[len(out)-1] += result[0]
                   } else {
                    baseindent, rest := grammar_separate_indent(out[len(out)-1])
                    baseindent += make_same_width_whitespace(rest)
                    out[len(out)-1] += result[0]
                    i := 1
                    for i < len(result) {
                        out = append(out, baseindent + result[i])
                        i += 1
                    }
                }
            }
            state = STATE_LOOK_FOR_LEADIN
        }
    }
    return out
}


func templates_expand_template(t string) ([]string, bool) {
    t, ok := strings.CutPrefix(t, "#")
    if ok {
        // This is a macro call
        return _templates_expand_call(t)
    }
    // Otherwise it is just a lookup
    v, ok := template_namespace.get(t)
    if !ok {
        return nil, false
    }
    typename := reflect.TypeOf(v).String()
    if typename == "main.annotated_value" {
        v = (v.(annotated_value)).value
        typename = reflect.TypeOf(v).String()
    }
    switch typename {
    case "string":
        return []string{v.(string)}, true
    case "[]string":
        return v.([]string), true
    case "int":
        return []string{fmt.Sprintf("%d",v.(int))}, true
    case "func() string":
        f := v.(func()string)
        return []string{f()}, true
    case "func() []string":
        f := v.(func()[]string)
        return f(), true
    case "func() int":
        f := v.(func()int)
        return []string{fmt.Sprintf("%d",f())}, true
    case "main.builtin_macro":
        return _templates_exec_builtin_noargs(v.(builtin_macro)), true
    case "main.macro":
        return _templates_exec_macro_noargs(v.(macro)), true
    }
    return nil, false
}


func _templates_exec_anything(id string, args [][]string) ([]string, bool) {
    // exec_anything together with non-removal of non-defined macros gives us some funny effects
    // This is due to the fact that the text is expanded one more time.
    v, ok := template_namespace.get(id)
    if !ok {
        return []string{""},false
    }
    typename := reflect.TypeOf(v).String()
    if typename == "main.annotated_value" {
        v = (v.(annotated_value)).value
        typename = reflect.TypeOf(v).String()
    }
    var ret []string
    switch typename {
    case "string":
        ret = append(ret, v.(string))
    case "[]string":
        ret = append(ret, v.([]string)...)
    case "int":
        return []string{fmt.Sprintf("%d",v.(int))}, true
    case "func() string":
        f := v.(func()string)
        return []string{f()}, true
    case "func() []string":
        f := v.(func()[]string)
        return f(), true
    case "func() int":
        f := v.(func()int)
        return []string{fmt.Sprintf("%d",f())}, true
    case "main.builtin_macro":
        return _templates_exec_builtin(v.(builtin_macro), args), true
    case "main.macro":
        return _templates_exec_macro(v.(macro), args), true
    default:
        return []string{""}, false
    }
    return templates_expand_lines(ret), true
}


func _templates_exec_builtin_noargs(m builtin_macro) []string {
    var args [][]string
    for _, v := range m.args {
        if strings.HasPrefix(v, "[") {
            break
        }
        args = append(args,[]string{""})
    }
    return m.handler(args)
}


func _templates_exec_macro_noargs(m macro) []string {
    template_namespace.enter()
    defer template_namespace.exit()
    for _,name := range m.args {
        template_namespace.define(name, "")
    }
    return templates_expand_lines(m.code)
}


func _templates_exec_builtin(m builtin_macro, args [][]string) []string {
    for len(args) < len(m.args) {
        i := len(args)
        if strings.HasPrefix(m.args[i],"[") {
            //[name] denotes optional parameter
            break
        }
        args = append(args, []string{""})
    }
    return m.handler(args)
}


func _templates_exec_macro(m macro, args [][]string) []string {
    template_namespace.enter()
    defer template_namespace.exit()
    for len(args) < len(m.args) {
        args = append(args, []string{""})
    }
    for i,name := range m.args {
        template_namespace.define(name, args[i])
    }
    return templates_expand_lines(m.code)
}


func _templates_expand_call(t string) ([]string, bool) {
    // We are NOT going to fully tokenize the call syntax.
    // This is not a decision set in stone, but the tokenization seems redundant in our simple case.
    // Inside expressions we have 'simplified spreadsheet syntax' - only functions are allowed, nested to any level
    // so we have one of:
    // JustAnIdentifier      -- means we would call it with empty argument list anyway
    // JustAnIdentifier()    -- same as before, but the call is explicit
    // JustAnIdentifier(...) -- we will process the arguments first and then do the call
    //log_debug("_templates_expand_call(" + t + ")")
    var out_args [][]string

    identifier, arglist, ok := _templates_split_call(t)
    if !ok {
        return []string{""}, false
    }
    
    if arglist != "" {
        // Working with arguments would actually be simple, if there would not be any nested calls. However, there are.
        // We need a state machine cutter for that
        arg_expressions, ok := grammar_split_macro_arguments(arglist)
        //log_debug(fmt.Sprintf("_templates_expand_call(): after expand: %q",arg_expressions))
        if !ok {
            return []string{""}, false
        }
        for _, expr := range arg_expressions {
            eres, ok := _templates_expand_call_arg(expr)
            if ok {
                out_args = append(out_args, eres)
            } else {
                return []string{""}, false
            }
        }
    }
    return _templates_exec_anything(identifier, out_args)
}


func _templates_expand_call_arg(expression string) ([]string, bool) {
    // This is a bit more complicated then _expand_call() counterpart:
    //  - String literals are allowed here (they start with doublequote though, we can detect them easily)
    //  - Number literals are allowed here
    //  - function calls are allowed here
    //  - and identifiers are allowed here
    // So:
    expression = strings.TrimSpace(expression)
    if strings.HasPrefix(expression, `"`) {
        s, ok := grammar_unqscape_string_literal(expression)
        if ok {
            return []string{s}, true
        }
    } else {
        s, ok := grammar_read_number(expression)
        if ok {
            return []string{s}, true
        }
        return _templates_expand_call(expression)
    }
    return []string{""}, false
}


func _templates_split_call(t string) (string, string, bool) {
    runes := []rune(t)
    const (
        IDENTIFIER_FIRST = iota
        IDENTIFIER_NEXT
        IN_ARGLIST
        FINAL_WHITESPACE
        MAYBE_POST_ARGLIST
    )

    state := IDENTIFIER_FIRST
    identifier_end := -1
    arglist_start := -1
    arglist_end := -1
    for i,r := range runes {
        switch state {
        case IDENTIFIER_FIRST:
            if !grammar_is_valid_identifier_first(r) {
                return t,"",false
            }
            state = IDENTIFIER_NEXT

        case IDENTIFIER_NEXT:
            if unicode.IsSpace(r) {
                state = FINAL_WHITESPACE
                identifier_end = i-1
            } else if r == rune('(') {
                state = IN_ARGLIST
                identifier_end = i-1
                arglist_start = i
            } else {
                if !grammar_is_valid_identifier_next(r) {
                    return t, "", false
                }
            }

        case IN_ARGLIST:
            if r == rune(')') {
                state = MAYBE_POST_ARGLIST
                arglist_end = i
            }

        case MAYBE_POST_ARGLIST:
            if !unicode.IsSpace(r) {
                if r != rune(')') {
                    state = IN_ARGLIST
                }
                arglist_end = i
            }

        case FINAL_WHITESPACE:
            if !unicode.IsSpace(r) {
                return t, "", false
            }
        }
    }

    switch state {
    case IDENTIFIER_FIRST:
        // bad
    case IDENTIFIER_NEXT:
        return t, "", true
    case IN_ARGLIST:
        // bad
    case MAYBE_POST_ARGLIST:
        return t[:identifier_end+1], t[arglist_start+1:arglist_end], true
    case FINAL_WHITESPACE:
        return t[:identifier_end], "", true
    }

    return t, "", false
}


func templates_init() {
    template_namespace.init(builtins_set_templates)
}


func templates_is_defined(name string) bool {
    _, ok := template_namespace.get(name)
    return ok
}
