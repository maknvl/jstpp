//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file contains functions dealing with file paths
//


package main


import (
	"path"
	"strings"
	"os"
)


func path_init() {
	global.initial_cwd, _ = os.Getwd()
}


func path_normalize(inpath string, basepath string) string {
	if (inpath == "-") || (inpath == "<args>") {
		return inpath
	}
    outpath := path.Clean(inpath)
    if !path.IsAbs(outpath) {
        outpath = path.Join(basepath, outpath)
    }
    return outpath
}


// make_relative_path takes an absolute current path and absolute base path
// and returns relative path to reach current path from base path if current
// path is inside base path directory or just returns current path if not
func path_make_relative(basepath string, curpath string) string {
    // Special case for our stdin designation
    if curpath == "-" {
        return "<stdin>"
    }
    if curpath == "<args>" {
    	return "<args>"
    }
    // When our current path is shorter than base path,
    // we are definitely not inside the base path
    if len(basepath) > len(curpath) {
        return curpath
    }
    // If current path does not start with basepath, we are not inside
    if !strings.HasPrefix(curpath, basepath) {
        return curpath
    }
    abase, _ := strings.CutPrefix(curpath, basepath)
    // If the leftover does not start with path separator, we are no inside
    if !strings.HasPrefix(abase, "/") {
        return curpath
    }
    // Remove the path separator (there may be multiple)
    for strings.HasPrefix(abase, "/") {
        abase, _ = strings.CutPrefix(abase, "/")
    }
    return abase
}
