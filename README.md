
# jstpp #

_jstpp_ is a simple **indentation-aware** template processor for text files with Unicode support, parametric macros and simple conditional processing.
 

## Features ##

 - symbol definitions via #define syntax, including parametric macros (but not variadic)
 - includes via #include
 - conditionals via #if, #ifdef, #ifndef, #else and #endif syntax
 - **indentation-aware** includes
 - **indentation-aware** template insertion and macro calls
 - #pragma once
 - small number of builtin text generation macros
 - Unicode support

_jstpp_ is suitable for preprocessing YAML, JSight, Python and other languages utilizing indents and Unicode.

_jstpp_ does NOT have a solid lexer, is not language-aware and operates only _on obvious line basis_.

When using a very limited number of features (i.e includes and conditionals), _jstpp_ is to some degree 
compatible with C Preprocessor. But don't do that, please. CPP is MUCH better suited for the task.


**The following documentation is incomplete and sometimes even wrong.**
**Check out [help.go](help.go) for more accurate docs.**

I shall fix the docs, promise


## Basic usage ##

### Syntax ###

_jstpp_ has _directives_ and _templates_.

_Directives_ are written like this:
~~~ c
#include "filename"
~~~
Directives are preceeded by a sharp character (`#`).
Before the directive only whitespace is allowed.

When jstpp processes a directive, it consumes the line. Even when the directive is invalid.
So, all lines that have some or none whitespace, followed by a sharp char, immediately followed by alphanumeric word will be considered as a directive and will be consumed by jestpp.

Directives may also span across multiple lines:
~~~ c
#define FOO Bar \
  Baz \
  Bzzz \
  Bux
~~~

_templates_ are used for text substitution. They are written like this:
~~~ py
import foo

foo.{{method}}( {{make_argument_list(10)}} ) # This is a comment with {{template}}
~~~
Here are 3 templates: `method`, `make_argument_list(10)` and `template`.
Note that jestpp does not understand ANY grammar, so it will process templates everywhere, including comments.
If, for example, `method` is defined as `bar`, `make_argument_list()` produces comma-separated list of latin alphabetic identifiers and `template` is defined as `LOVE`, then after jestpp processing the above example would translate into
~~~ py
import foo

foo.bar( a,b,c,d,e,f,g,h,i,j ) # This is a comment with LOVE
~~~

Unlike directives, templates are NOT consumed. When a template is used that is not defined, it is left as it was. When `foo` has a value of `x`, but `bar` is not defined, this:
~~~
{{foo}} {{bar}}
~~~
will become this after processing:
~~~
x {{bar}}
~~~


### Includes ###

Includes do just that - include content of some other file.

The CPP semi-compatible `#include` directive does just that - includes the contents of other file.
The basic syntax is:
~~~ cpp
#include "path/to/file"
~~~
The content of the specified file gets preprocessed and included into the file with the same indentation level. That is, when `a.txt` contains:
~~~
no indent
  some indent
    more indent
~~~
and `b.txt` contains:
~~~
This is b.txt
  This portion is indented
    #include "a.txt"
~~~
The resulting output will be:
~~~
This is b.txt
  This portion is indented
    no indent
      some indent
        more indent
~~~


### Defining symbols with `#define` ###

Using `#define` directive, a symbol can be defined or redefined.

Without specified value, a symbol is defined to be an empty string.
~~~ c
#define FOO
~~~

A value can be specified
~~~ c
#define FOO "bar"
~~~

_jstpp_ treats all non-quoted values as strings.
~~~ c
#define FOO 1
#define BAR 2 // Comment
#define BAZ "text"
#define FOOBAR    something and something else
#define BUX "text" and then someting else
#define DUX "text" // and a comment
FOO:    {{FOO}}
BAR:    {{BAR}}
BAZ:    {{BAZ}}
FOOBAR: {{FOOBAR}}
BUX:    {{BUX}}
DUX:    {{DUX}}
~~~
Produces:
~~~
FOO:    1
BAR:    2 // Comment
BAZ:    text
FOOBAR: something and something else
BUX:    text
DUX:    text
~~~

A parametric macro can be defined:
~~~ c
#define COMBINE(X,Y)  {{X}}{{Y}}
{{#COMBINE("foo","bar")}}
~~~
Results in:
~~~
foobar
~~~

Parametric macros can take other symbols as arguments
~~~ c
#define MULTIPLE(X)  {{X}}s
#define FRUIT        apple
#define COMBINE(X,Y) {{X}}{{Y}}
{{#COMBINE("sweet",MULTIPLE(FRUIT))}}
~~~
Produces:
~~~
sweet apples
~~~


### Conditionals ###

Conditional processing is available in a number of forms.
`#ifxxx` directive starts a condition check. Following lines up to #else or #endif are processed if the condition holds, and lines between corresponding #else and #endif are discarded. If condition does not hold, and #else block is available, lines after #else are processed until corresponding #endif.

#### `#ifdef` ####

Checks whether the symbol is defined.

Syntax:
~~~ c
#ifdef FOO
This gets processed if FOO is defined
#else
This gets processed if FOO is NOT defined
#endif
~~~

#### `#ifndef` ###

Checks whether the symbol is NOT defined.


#### `#if` ####

Performs macro resolution on it's argument and checks whether the result is truthy.

Syntax:
~~~ c
#if MACRO
~~~


## Other ###




#### `#undef` ###
