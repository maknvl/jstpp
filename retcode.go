//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// This file contains exitcode stuff
//


package main


import (
	"os"
)


// retcode is used to define exit codes for app
type retcode int

const (
	ESUCCESS retcode = iota  // Successful completion
    EBADARGS                 // Bad command line arguments
    ESYNTAX                  // Syntax error in directives, templates, etc.
    ESYSTEM                  // System error: read or open failed, etc.
)

var rc_override retcode = ESUCCESS


// override_rc() is used to set an exit code for the future exit by error handling routines.
// This is useful since we have a report-and-continue policy for all errors, except fatal.
// The highest reported return code is returned at the end.
func override_rc(rc retcode) {
    if rc > rc_override {
        rc_override = rc
    }
}


func exit(rc retcode) {
	override_rc(rc)
	os.Exit(int(rc_override))
}
