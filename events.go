//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// In this file we are dealing with errors and events that happen during the processing of files.
//


package main


import (
    "os"
    "fmt"
)


// Each event has a handling action, that defines what should be done when the event is encountered.
// Basically, this controls errors and warnings.
type action_t int

const (
    ACTION_IGNORE action_t = iota // Ignore the event completely, no warnings or errors
    ACTION_WARNING                // Produce a warning and continue (exit code is unchanged)
    ACTION_ERROR                  // Produce an error and continue (exit code is set to non-zero value)
    ACTION_FATAL                  // Produce an error and stop processing
)


// Event rules provide default actions and descriptions for each event id
type event_rule_t struct {
    default_action action_t   // Default action for the event
    action         action_t   // Currently set action (via command-line options)
    description    string     // Description for the event for generated help
}

var events map[string]event_rule_t


func events_init() {
    events = make(map[string]event_rule_t)
    rule := func(id string, set action_t, desc string) {
        events[id] = event_rule_t{
            default_action: set,
            action: set,
            description: desc,
        }
    }
    rule( "unknown-option",                  ACTION_ERROR,   "Unknown option in cmdline args" )
    rule( "unknown-parameter",               ACTION_WARNING, "Unknown parameter used with -f option" )
    rule( "readonly-parameter",              ACTION_WARNING, "Readonly parameter used with -f option" )
    rule( "invalid-parameter-value",         ACTION_WARNING, "Invalid parameter value")
    rule( "unknown-event",                   ACTION_WARNING, "Unknown event specified for -H option" )
    rule( "invalid-event-action",            ACTION_WARNING, "Invalid action used with -H option" )
    rule( "builtin-redefined",               ACTION_WARNING, "Builtin redefined by directive" )
    rule( "builtin-undefined",               ACTION_WARNING, "Builtin undefined by directive")
    rule( "file-open-failed",                ACTION_ERROR,   "Could not open file")
    rule( "explicit-error",                  ACTION_ERROR,   "#error directive encountered")
    rule( "explicit-warning",                ACTION_WARNING, "#warning directive encountered")
    rule( "unknown-pragma",                  ACTION_WARNING, "Unknown option for #pragma")
    rule( "invalid-pragma-value",            ACTION_WARNING, "Invalid #pragma value")
    rule( "unknown-directive",               ACTION_ERROR,   "Unknown directive")
    rule( "file-read-failed",                ACTION_ERROR,   "File read failed")
    rule( "multiline-define-broken-indent",  ACTION_WARNING, "Bad indent in multiline #define")
    rule( "multiline-define-shorter-indent", ACTION_WARNING, "Indent too short in multiline #define")
    rule( "empty-ifdef",                     ACTION_ERROR,   "Missing template name in #ifdef")
    rule( "unexpected-else",                 ACTION_ERROR,   "Unexpected #else")
    rule( "unexpected-endif",                ACTION_ERROR,   "Unexpected #endif")
    rule( "multiline-define-left-hanging",   ACTION_ERROR,   "End of file during multiline #define")
    rule( "if-left-hanging",                 ACTION_ERROR,   "Missing #endif (or several)")
    rule( "define-failed",                   ACTION_ERROR,   "Syntax error in definition")
    rule( "if-macro-failed",                 ACTION_ERROR,   "Macro expansion failed in #if")
}


// events_set_action() sets action for an existing event
func events_set_action(event string, action action_t) bool {
    old_value, ok := events[event]
    if !ok {
        // Only existing events can be modified
        return false
    }
    old_value.action = action
    events[event] = old_value
    return true
}


// events_reset_action() resets action for an existing event
func events_reset_action(event string) bool {
    old_value, ok := events[event]
    if !ok {
        return false
    }
    old_value.action = old_value.default_action
    events[event] = old_value
    return true
}


// events_reset_all() resets all events to their default actions
func events_reset_all() {
    for id, value := range events {
        value.action = value.default_action
        events[id] = value
    }
}


// events_exec() executes the event rule for a specified event id
// Typical usage would be:
// ```
// func specific_event() {
//   return events_exec("my-specific-event-id", "Something is broken", ESYNTAX)  
// }
// ...
// rc := specific_event() // Deals with event processing rules and does logging if necessary
// if rc != ESUCCESS {
//   do_exit_procedure()
// }
// // otherwise continue
// ```
func events_exec(id string, message string, erc retcode) retcode {
    rule, _ := events[id]
    switch rule.action {
    case ACTION_IGNORE:
    case ACTION_WARNING:
        log_in_context(WARNING, message)
    case ACTION_ERROR:
        log_in_context(ERROR, message)
        override_rc(erc)
    case ACTION_FATAL:
        log_in_context(FATAL, message)
        return erc
    }
    return ESUCCESS
}


// Specific error handling functions
// These get called when corresponding event occurs. Some of them concern only the command-line options.
// Those of handlers that can be configured (action can be changed via command line), have names starting with `event_`
// Those that can not be configured, start with action that is going to take place.  

func fatal_filename_not_specified() retcode {
    // Absense of filename is always fatal
    log_fatal("Filename not specified!")
    return EBADARGS
}


func fatal_unknown_help_topic(topic string) retcode {
    log_fatal("Unknown help topic: " + topic)
    return ESYNTAX
}


func error_option_argument_missing() retcode {
    // When an option argument is missing, filename would also be missing
    log_error("Argument for " + os.Args[context.linenum] + " is missing")
    override_rc(EBADARGS)
    return ESUCCESS
}


func event_define_failed(name string, value string) retcode {
    return events_exec( "define-failed", "Bad define: " + name + " -> " + value, EBADARGS)
}


func event_unknown_option() retcode {
    return events_exec( "unknown-option", "Unknown option: " + os.Args[context.linenum], EBADARGS)
}


func event_unknown_parameter(name string) retcode {
    return events_exec("unknown-parameter", "Unknown parameter: " + name, EBADARGS)
}


func event_readonly_parameter(name string) retcode {
    return events_exec("readonly-parameter", "Parameter can not be set: " + name, EBADARGS)
}


func event_invalid_parameter_value(name string, value string) retcode {
    return events_exec("invalid-parameter-value", "Invalid parameter value for " + name + ": " + value, EBADARGS)
}


func event_unknown_event(name string) retcode {
    return events_exec("unknown-event", "Unknown event: " + name, EBADARGS)
}


func event_invalid_action(event string, action string) retcode {
    return events_exec("invalid-event-action", "Invalid action for event " + event + ": " + action, EBADARGS)
}


func event_builtin_redefined(name string) retcode {
    return events_exec("builtin-redefined", "Builtin " + name + " redefined", EBADARGS)
}


func event_builtin_undefined(name string) retcode {
    return events_exec("builtin-undefined", "Builtin " + name + " undefined", ESYSTEM)
}


func fatal_include_depth_reached(filepath string) retcode {
    log_fatal(fmt.Sprintf("Max include depth (%d) reached", global.max_include_depth))
    return ESYSTEM
}


func event_file_open_failed() retcode {
    return events_exec( "file-open-failed", "Failed to open the file", ESYSTEM)
}


func event_explicit_error(message string) retcode {
    return events_exec("explicit-error", message, ESYSTEM)
}


func event_explicit_warning(message string) retcode {
    return events_exec("explicit-warning", message, ESYSTEM)
}


func event_invalid_pragma_value(value string) retcode {
    return events_exec("invalid-pragma-value", "Invalid #pragma value: " + value, ESYNTAX)
}


func event_unknown_pragma(pragma string) retcode {
    return events_exec("unknown-pragma", "Unknown pragma: " + pragma, ESYNTAX)
}


func event_unknown_directive(directive string) retcode {
    return events_exec("unknown-directive", "Unknown directive: " + directive, ESYNTAX)
}


func event_file_read_failed() retcode {
    return events_exec("file-read-failed", "Read file failed", ESYSTEM)
}


func event_empty_define() retcode {
    return events_exec("empty-define", "No arguments provided for define directive", ESYNTAX)
}


func event_multiline_define_broken_indent(name string) retcode {
    return events_exec(
        "multiline-define-broken-indent",
        "Indentation mismatched in multi-line define of " + name,
        ESYNTAX)
}


func event_multiline_define_shorter_indent(name string) retcode {
    return events_exec(
        "multiline-define-shorter-indent",
        "Indentation is too short in multi-line define of " + name,
        ESYNTAX)
}


func event_empty_if() retcode {
    return events_exec("empty-if", "Missing argument for #ifxxx directive", ESYNTAX)
}


func event_unexpected_else() retcode {
    return events_exec("unexpected-else", "Unexpected #else", ESYNTAX)
}


func event_unexpected_endif() retcode {
    return events_exec("unexpected-endif", "Unexpected #endif", ESYNTAX)
}


func event_multiline_define_left_hanging(name string) retcode {
    return events_exec("multiline-define-left-hanging", "Unexpected end of file in multiline #define", ESYNTAX)
}


func event_if_left_hanging() retcode {
    return events_exec("if-left-hanging", "Unfinished #ifxxx directive (missing #endif?) at the end of file", ESYNTAX)
}


func event_if_macro_failed() retcode {
    return events_exec("if-macro-failed", "Macro expansion failed while processing #if directive", ESYNTAX)
}
