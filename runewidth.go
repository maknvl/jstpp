//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// ----------------------------------------------------------------------------
//
// This file contains functions dealing with the monospace width (cell width) of unicode glyphs
//


package main


import (
    "strings"
    runewidth "github.com/mattn/go-runewidth"
)


// make_same_width_whitepace() takes a string and returns a string where all characters that are not whitespace or tabs
// are replaced by spaces. Characters that have monospace width 2 are replaced by 2 spaces.
// Tabs are left as they are.
func make_same_width_whitespace(s string) string {
    sections := strings.Split(s,"\t")
    w := runewidth.StringWidth(sections[0])
    out := strings.Repeat(" ", w)
    i := 1
    for i < len(sections) {
        w = runewidth.StringWidth(sections[i])
        out += "\t" + strings.Repeat(" ",w)
        i += 1
    }
    return out
}


func cellwidth(s string, tabsize int) int {
    // It would have been nice to think that tabs are just like a fixed number of spaces.
    // However, it is not so. Tabs are evil.
    // A tab just moves you up to tabsize. If one is set.
    if tabsize <= 1 {
        return runewidth.StringWidth(s)
    }
    sections := strings.Split(s,"\t")
    pos := runewidth.StringWidth(sections[0])
    i := 1
    for i < len(sections) {
        tablen := tabsize - (pos - (pos/tabsize)*tabsize)
        pos += tablen + runewidth.StringWidth(sections[i])
        i += 1
    }
    return pos
}
