//
// This file is part of jstpp. See jstpp.go for more info.
//
// @license MIT
//
// -------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2023 Mikhail A. Konovalov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions
// of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
// -------------------------------------------------------------------------------------------------------------------
//
// This file deals with parameters. Most of the code here is redundant, as only getter/setter parameters are now used.
//


package main


import (
	"reflect"
	"strconv"
)

// Parameters map holds various modifiable parameters of the processing engine.
// These can be changed via command-line options.
// While typed as any, actual parameter values are either positive int or non-empty string or special getter/setter
// structures, that operate on non-empty strings internally.
type parameters_t struct {
	values map[string]any
	init_f func(map[string]any)
}


type parameters_op_result int

const (
	PARAMETER_INVALID = iota + 1
	PARAMETER_INVALID_VALUE
	PARAMETER_READONLY
)


// Getter/Setter struct allows to attach parameter to any value in the global or local state
type getset struct {
	get func() any
	set func(string) bool
}


func (p* parameters_t) init(initializer func(map[string]any)) {
	p.init_f = initializer
	p.values = make(map[string]any)
	p.init_f(p.values)
}


func (p* parameters_t) set(name string, value string) parameters_op_result {
	// We employ the fun way of name and type control
	old_value, ok := p.values[name]
	if (!ok) {
		// We can't find it = you can't update it
		return PARAMETER_INVALID
	}
	if value == "" {
		// Empty values are not permitted
		return PARAMETER_INVALID_VALUE
	}
	typename := reflect.TypeOf(old_value).String()
	switch typename {
	case "int":
		intval, err := strconv.Atoi(value)
        if err != nil {
        	// Can't set integer param to non-integer value
            return PARAMETER_INVALID_VALUE
        }
        if intval <= 0 {
        	// Negative and zero params are not supported
        	return PARAMETER_INVALID_VALUE
        }
        p.values[name] = intval
    case "string":
    	p.values[name] = value
    case "main.getset":
    	t := old_value.(getset)
    	if !t.set(value) {
    		return PARAMETER_INVALID_VALUE
    	}
    	return 0
    default:
    	// Consider all other types to be non-updateable
    	return PARAMETER_READONLY
	}
	return 0
}


func (p* parameters_t) get(name string) (any, bool) {
	v, ok := p.values[name]
	if ok {
		t := reflect.TypeOf(v).String()
		if t == "main.getset" {
			vx := v.(getset)
			return vx.get(), true
		}
	}
	return v, ok
}


func (p* parameters_t) get_int(name string) (int, bool) {
	value, ok := p.get(name)
	if !ok {
		return 0, false
	}
	typename := reflect.TypeOf(value).String()
	if typename != "int" {
		return 0, false
	}
	return value.(int), true
}


func (p* parameters_t) get_string(name string) (string, bool) {
	value, ok := p.get(name)
	if (!ok) {
		return "", false
	}
	typename := reflect.TypeOf(value).String()
	if typename != "string" {
		return "", false
	}
	return value.(string), true
}


func (p* parameters_t) reset() {
	p.init_f(p.values)
}


func getset_wrap_int(pvar *int) getset {
	return getset{
		get: func() any {
			return *pvar
		},
		set: func(s string) bool {
			v, err := strconv.Atoi(s)
			if err != nil {
				return false
			}
			*pvar = v
			return true
		}}
}


func getset_wrap_string(pvar *string) getset {
	return getset{
		get: func() any {
			return *pvar
		},
		set: func(s string) bool {
			*pvar = s
			return true
		}}
}
